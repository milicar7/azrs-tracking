# azrs-tracking

Projekat nad kojim sam primenila alate: [Monopol](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/03-monopol)

Na projektu smo koristili `gitflow` model grananja.

## Alati:

- [GDB](https://gitlab.com/milicar7/azrs-tracking/-/issues/2)

- [git hook](https://gitlab.com/milicar7/azrs-tracking/-/issues/1)

- [Valgrind](https://gitlab.com/milicar7/azrs-tracking/-/issues/3)

- [Gammaray](https://gitlab.com/milicar7/azrs-tracking/-/issues/8)

- [ClangFormat](https://gitlab.com/milicar7/azrs-tracking/-/issues/7)

- [Staticka analiza koda](https://gitlab.com/milicar7/azrs-tracking/-/issues/4)

- [Docker](https://gitlab.com/milicar7/azrs-tracking/-/issues/9)

- [GCov](https://gitlab.com/milicar7/azrs-tracking/-/issues/6)

- [Ostali alati](https://gitlab.com/milicar7/azrs-tracking/-/issues/5)
